package exo2;

import java.util.Scanner;

public class Exo2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Calculatrice seconde méthode
		int choix = 0;
		int[] num = new int[3];
		char operation = ' ';
		
		Scanner monScanner = new Scanner(System.in);
		
		do {
		
		String menu = "\n         Menu Principal \n\n   1. Additionner 2 nombres\n   2. Soustraire 2 nombres\n   3. Multiplier 2 nombres \n   4. Diviser 2 nombres\n   0. Quitter\n\n Votre choix :";
		System.out.println(menu);
		
		choix = monScanner.nextInt();
		
		if(choix !=0){
			for(int i = 0; i<2; i++){
				System.out.println("Saisir nombre " + (i+1) + " : ");
				num[i] = monScanner.nextInt();
				monScanner.nextLine();
			}
			switch (choix) {
			case 1 :
				num[2] = num[0] + num[1];
				operation = '+';
				break;
			case 2 :
				num[2] = num[0] - num[1];
				operation = '-';
				break;
			case 3 :
				num[2] = num[0] * num[1];
				operation = '*';
				break;
			case 4 :
				if (num[1] != 0){
					num[2] = num[0] / num[1];
				} else {
					num[2] = 2147483647;
				}
				operation = '/';
				break;
			}
			System.out.println("Le résultat de " + num[0] + operation + num[1] + " est : " + num[2]);
		}
	
		} while (choix != 0);
		
		monScanner.close();
	
	}

}
