package exo4;

public class Exo4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Écrivez un programme Java pour additionner les valeurs d'un tableau.
		
		int[] tabIn = {12,24,56,11,14,12,0,4,9,10};
		int res = 0;
		
		for(int i=0; i < tabIn.length; i++) {
			res += tabIn[i];
		}
		System.out.println("La somme du tableau est de : " + res);
	}

}
