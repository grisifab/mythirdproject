package exo5;

import java.util.Scanner;

public class Exo5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Écrivez un programme Java qui demande à un utilisateur de remplir un tableau et de l’affiche par la suite
		int i = 1;
		int[] tab = new int[20];
		boolean finTableau = false;
		
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Veuillez renseigner les valeurs entières du tableau");
		System.out.println("Pour indiquer la fin du tableau (=< 20 valeurs), tapper fin");
		do {
			System.out.println("Valeur N° " + i + " :");
			String s1 = monScanner.next();
			monScanner.nextLine(); // pour vider le scanner
			if (s1.contains("fin")){
				finTableau = true;
			} else {
				tab[(i-1)] = Integer.parseInt(s1);
				i++;
			}	
		}while (!finTableau && i<=20);
		
		for (int j = 0; j < (i-1); j++) {
			System.out.println(tab[j]);
		}
		monScanner.close();
	}

}
